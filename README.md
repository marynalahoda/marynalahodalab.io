[lahoda.bio](http://lahoda.bio)

Марина это твой сайт. Заливай сюда свои публичные файлы, публичные документы, резюмэ.

# Active Page of Maryna Lahoda

Web application developed by [Next.js](https://nextjs.org/) and integrated into [Netlify CMS](https://www.netlifycms.org/)

## Links

+ [website](https://lahoda-bio.netlify.com/)

+ [admin panel](https://lahoda-bio.netlify.com/admin/#/collections/pages)

Publish wait links

https://gitlab.com/marynalahoda/marynalahoda.gitlab.io/pipelines

https://app.netlify.com/sites/lahoda-bio/deploys?filter=master

## Used packages
+ [Next-Seo](https://github.com/garmeeh/next-seo)
+ [Styled components](https://www.npmjs.com/package/styled-components)
+ [next-images](https://www.npmjs.com/package/next-images)
+ [frontmatter-markdown-loader](https://www.npmjs.com/package/frontmatter-markdown-loader)

## How make changes

Run local instance (see URL in console):

```sh
npm install
npm run dev
```

> At any change into master theof repository, the site is automatically collected and uploaded to the hosting

## File structure

+ Image download in folder **public/img**

+ Markdown files  in folder - **content**

+ Configuration Web App - **next.config.js**

+ Work configuration with markdown files - **config.yml**

## Notes

If you need to add another block to the main page, you need to uncomment the component **undefined** and write the necessary text in the corresponding markdown file.


## Аналитика поиска

https://ads.google.com/aw/express/dashboard?campaignId=9567038849&ocid=413303714&__u=5818373178&__c=6795623186