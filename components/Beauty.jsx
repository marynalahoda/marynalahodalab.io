import { attributes, react as MarkdownContent } from '../content/beauty.md';
import styled from 'styled-components'

export const Beauty = () => {
    let { title, thumbnail } = attributes;

    return (
        <StyledBeauty>
            <h3>{title}</h3>
            <img src={thumbnail} />
            <div><MarkdownContent/></div>
        </StyledBeauty>
    )
}

const StyledBeauty = styled.div`
        max-width: 280px;
        align-items: center;
        padding: 4%;
        position: relative;
        
        img{
            object-fit: cover;
            position: absolute;
            margin-top: 3.5%;
            box-shadow: 0 0 13px 3px rgba(0,0,0,.5);
        }
        h3{
            margin: 0;
            text-align: center;
            height: 70px;
            font-size: calc(15px + 0.3vw);

        }

        div {
            text-align: justify;
            min-width: 280px;
            margin-top: 340px;

        }
        ul{
            color: rgba(29,29,29,.7);
            font-weight: 400;
            font-style: normal;
            font-size: calc(12px + 0.5vw);
            letter-spacing: .04em;
            line-height: 1.4em;
        }
`