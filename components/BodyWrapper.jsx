import styled from 'styled-components'
import { Footer } from './Footer'

export const BodyWrapper = (props) => {
    return(
        <Body>

            {props.children}
            <Footer />
        </Body>
    )
}

const Body = styled.div`
    max-width: 1440px;
    margin: 0 auto;
    width: 90%;
`
