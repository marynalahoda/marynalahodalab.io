import styled from 'styled-components'
import React from 'react'

// TODO: move locations and contacts into MD or YML
export const Footer = () => {
    return (
        <FooterStyled>
            <hr />
            <div>
                <div>
                    <h4>Contact</h4>
                    <a href="mailto:marynalahoda@gmail.com">marynalahoda@gmail.com</a>
                </div>
            </div>
        </FooterStyled>
    )
}

// TODO: use # color every where and remove most of PX to make all scalable
const FooterStyled = styled.footer`
    width: 100%;
    height: 350px;
    margin: 0 auto;
    margin-top: 20px;
    display: flex;
    justify-content:center;
    align-items: center;
    flex-direction: column;

    hr{
        background-color: rgba(29,29,29,.3);
        height: 1px;
        width: 80%;
    }

    div {
        width: 80%;
        display: flex;
        justify-content: space-around;
        flex-wrap: wrap;
        
        @media screen and (max-width: 1000px){
            justify-content: center;
      }
        div{
            max-width: 250px;
            display: flex;
            align-items: center;
            flex-direction: column;
            justify-content: flex-start;
            h4{
                font-family: Rubik;
                font-weight: 400;
                font-style: normal;
                font-size: 24px;
                letter-spacing: .11em;
                line-height: 1.2em;
                text-transform: uppercase;
                color: #828282;
                text-align: center;
            }
            span, a{
                font-weight: 400;
                font-style: normal;
                font-size: 120%;
                color: rgba(29,29,29,.3);
                padding: 15px 0;
                text-decoration: none;
                text-align: center;
            }
        }
    }
`
