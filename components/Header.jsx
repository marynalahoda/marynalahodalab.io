import styled from 'styled-components'
import Link from "next/link";

class Header extends React.Component{

    render(){
        return (
            <HeaderStyled>
            <div>
                <Link href="/">
                    <a>Home</a>
                </Link>

                <Link href="/resume">
                    <a>Resume</a>
                </Link>
            </div>

            <div>
                <a href='tel:+375295066757'>+375 29 506 67 57</a>

                <section>
                <a href="https://telegram.im/@maryna_lahoda" target="_blank">
                        <img src={require('../data/icons/telegram.svg')}/>
                    </a>
                    <a href="viber://add?number=%2B375295066757">
                        <img src={require('../data/icons/viber.svg')}/>
                    </a>
                    <a href="https://wa.me/375295066757">
                        <img src={require('../data/icons/whatsapp.svg')}/>
                    </a>
                    <a href="mailto:marynalahoda@gmail.com">
                        <img src={require('../data/icons/gmail.svg')}/>
                    </a>
                    <a href="https://www.instagram.com/marynalahoda/">
                        <img src={require('../data/icons/instagram.svg')}/>
                    </a>
                    <a href="https://www.linkedin.com/in/maryna-lahoda/">
                        <img src={require('../data/icons/linkedin.svg')}/>
                    </a>
                    <a href="https://www.researchgate.net/profile/Maryna_Lahoda">
                        <img src={require('../data/icons/researchgate-logo.svg')}/>
                    </a>
                </section>
            </div>
        </HeaderStyled>
        )
    }
}
export default Header

const HeaderStyled = styled.header`
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;
    flex-wrap: wrap;
    font-family: Rubik;
    font-weight: 400;
    font-style: normal;
    font-size: 14px;
    letter-spacing: .16em;
    text-transform: uppercase;

    section {
        height: 120%;
        display:flex;
            @media screen and (max-width: 500px){
                justify-content: space-around;
                width: 100%;
                margin-top: 15px;
        }
    }

    a{
        text-decoration: none;
        color: #000;
        font-size: 1.5em;
        &:not(:last-child){
            margin-right: 15px;
            @media screen and (max-width: 500px){
                margin-right: 0;
            }
        }

        img{
            height: 23px;
            width: 23px;
        }
    }

    div{
        padding: 10px 0;
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: space-between;

        &:not(:first-child){
            @media screen and (max-width: 500px){
                justify-content: center;
                flex-direction: column;
            }
        }  
    
    h2{
        @media screen and (max-width: 500px){
            text-align: center;
        }
    }
}
`
