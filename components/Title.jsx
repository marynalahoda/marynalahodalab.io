import { attributes, react as TitleContent } from '../content/title.md';
import styled from 'styled-components'

const Title = () => {
    let { title } = attributes;

    return (
          <StyledHeading><h1>{title}</h1></StyledHeading>
    );
}
export default Title;

const StyledHeading = styled.div`
    h1{
    font-family: 'Baskervville', san-serif;
    font-weight: 400;
    font-style: normal;
    font-size: calc(30px + 0.5vw);
    letter-spacing: .11em;
    line-height: 1.2em;
    text-align: center;
    width: 100%;
    }
`