import { attributes, react as BaeutyContent } from '../content/undefined.md';

export const Undefined = () => {
    let { text, title, thumbnail } = attributes;

    return (
        <div>
            <h3>{title}</h3>
            <img src={thumbnail} />
            <p>{text}</p>
        </div>
    )
}