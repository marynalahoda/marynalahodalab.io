---
title: Maryna Lahoda PHD
thumbnail: /img/lahoda-resume.jpg
---

## Maryna Lahoda, PhD

I like to learn new things and apply them in practice. My interests are in biology, biophysics, cosmetology, medicine, biochemistry, cell biology, epidemiology, general biology, genetics, microbiology, molecular biology, pharmacology, physiology, toxicology. I have experiences from deep science to field work in medical and biologic related enterprises. I am motivated person to expand my skills and knowledge to get things done.

### Education

#### 2016/09-2018/07. [Nurse specialist with honors](/assets/docs/diploma_nurse_IMG_20200216_115820.jpg)

At [Minsk State Medical College](http://msmc.by/spetsialnost-sestrinskoe-delo) A nurse specialist with a secondary medical education, working under the guidance of a doctor in a medical and children's institution.

#### 2016/02-2016/05 [Cosmetic](/assets/docs/cosmetic_diploma_P60722-154642.jpg)

At [Meleor-Cosmecs LTD](http://meleor.by/esthetist-4-grade/), Minsk.

#### 2008 – 2013 [PhD in biophysics](/assets/docs/20140331_phd_diplom.jpg)

At [University of South Bohemia in České Budějovice, Faculty of Science](https://en.wikipedia.org/wiki/University_of_South_Bohemia_in_České_Budějovice), [Faculty of Fisheries and Protection of Waters, Research Center of Aquaculture and Biodiversity of Hydrocenoses](http://www.frov.jcu.cz/en/cenakva), Department of Structure and Function of Proteins, Institute of Physical Biology (now Institute of Complex Systems), Laboratory of Crystallogenesis and Biomolecular Crystallography, Czech Republic.

[Thesis 'Crystallographic study of haloalkane dehalogenase DhaA mutant variant DhaA12 and DhaA31 and analysis of substrate biodegradation by DhaA31'](assets/works/PhD_thesis_ML_31.06.12.pdf).

Research topics where - macromolecular crystallization , macromolecular crystallography, high-resolution, structure refinement.

#### 2003-2008 [Mgr diploma in Biology and Chemistry](/assets/docs/biology_diploma_1_20140331_123057.jpg)

At Department of Botany, Faculty of Biology, Belarusian State University, Minsk, Belarus.  

Diploma title 'Influence a SHF (super high frequency), preparations Mycosan and Biopag on a vegetative and reproductive functions of pathogenic micromycetes'.

Average grade is 7.3 of 10.

#### 2007/01-2007/04 [Landscape designer. Interior Designer.](assets/docs/design_diploma_20140331_121515.jpg)

At European Education Center "Viva Liberty"

### Work experience

#### 2020/10-now. Scrub nurse at Republican hospital of the Ministry of Internal Affairs

#### 2018 autumn - now. Entrepreneur experiences in pharmaceutics and medical services

#### 2018 spring - summer. Medical practice

#### 2016/09-2017/06. [Beauty salon Izumi](http://изуми.бел/). Hardware cosmetology, cosmetology.

#### 2017/06-2017/07. Practice in [City Clinical Clinic №5, Minsk](http://www.5gp.by/). Surgery Department №1.

#### 2008 – 2013 Scientific research work at the Laboratory of Crystallogenesis and Biomolecular Crystallography of Institute of Physical Biology University of South Bohemia Ceske Budejovice, Czech Republic.

Galoalkan degalogenazy as an object of structural studies FFPW USB, Institute of complex systems. Member of Laboratory of macromolecular structure and dynamics. Mentoring summer school students. Collaboration in distributed international research. Independent goal oriented research.

#### 2010/02 Institute of Biochemistry, University of Luebeck, Ratzeburger Allee 160.

#### 2009/10 Intership in Loschmidt Laboratories, Masaryk University, Brno, Czech Republic.

#### 2003 – 2008 Scientific research work at the Department of Botany of BSU, Belarus

### Publications

* Have done next depositions into [RCSB protein data bank](http://www.rcsb.org):  
  * [3RK4 Structure of Rhodococcus rhodochrous haloalkane dehalogenase mutant DhaA31](http://www.rcsb.org/pdb/explore/explore.do?structureId=3RK4)  
  * [4FWB Structure of Rhodococcus rhodochrous haloalkane dehalogenase mutant DhaA31 in complex with 1, 2, 3 - trichloropropane](http://www.rcsb.org/pdb/explore/explore.do?structureId=4FWB)  
  * [Structure of haloalkane dehalogenase DhaA from Rhodococcus rhodochrous](http://www.rcsb.org/pdb/explore/explore.do?structureId=4HZG) .

* "Dynamics and hydration explain failed functional transformation in dehalogenase design" Nature Chemical Biology 10(6) · April 2014

* "Crystallographic analysis of 1,2,3-trichloropropane biodegradation by the haloalkane dehalogenase DhaA31” Acta Crystallographica Section D Biological Crystallography 70(Pt 2):209-217 · February 2014

* "Crystallization and crystallographic analysis of the Rhodococcus rhodochrous NCIMB 13064 DhaA mutant DhaA31 and its complex with 1,2,3-trichloropropane" Acta Crystallographica Section F Structural Biology and Crystallization Communications 67(Pt 3):397-400 · March 2011

* "Influence a preparation Mycosan for pathogenic micromycetes” Sviatlova (Lahoda) M.V., PoliksenovaV.D, Fedorovic M.N. material of V International scince‑practical conference, 28‑30 November, 2007, Minsk / National Academy of Sciences Republic of Belarus. The regulation of growth activity, expansion and producing capacity of plant.

### Presentations

* 2010-02 Institute of Biochemistry, University of Luebeck, Germany Presentation, Poster “Structural analysis of DhaA12 mutant of the haloalkane dehalogenase from Rhodococcus rhodochrous NCIMB 13064”,
* 2010-06 42th crystallographic course at Ettore Majorana Centre, workshop, Erice, Italy Presentation “Structural analysis of mutated DhaA protein from Rhodococcus rhodochrous”,
* 2010-09 Heart of Europe bio-Crystallography Meeting, Schoeneck, Germany

### Computer skills

Long time user of Windows solving daily working tasks (preparing reports and papers, authoring graphics) using MS Office, LibreOffice, Paint.Net, Using Linux with console applications to solve scientific tasks (CCP4, Shelxl, PyMol). Internet research.

### Languages

Russian, Belarusian - native, English - working/above average read/write/speak, Czech - basic speak, read and write.

### Other info

Born in 1985 year in Smorgon of Belarus, married by [Dzmitry Lahoda](lahoda.pro), maiden name is Sviatlova, have one son Miroslav as of 2014, driver license type B, russian name is "Лагода Марина Валерьевна".

### References

* Dr. Ivana Kuta Smatanova
* Dr. Jeroen R. Mesters
* Dr. Jiri Damborsky, Masaryk University Brno, Director
* Prof. Поликсенова Валентина Дмитриевна , Belarusian State University, Зав. Кафедрой ботаники
