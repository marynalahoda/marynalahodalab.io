const withImages = require('next-images');
const withMDX = require('@next/mdx')();


module.exports = withImages(withMDX({
    webpack: (cfg) => {
        cfg.module.rules.push(
            {
                test: /\.md$/,
                loader: 'frontmatter-markdown-loader',
                options: { mode: ['react-component'] }
            }
        )
        return cfg;
    },
    pageExtensions: ['js', 'jsx', 'md', 'mdx']
}));