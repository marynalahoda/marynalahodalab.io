import Head from "next/head"
import { Component } from 'react'
import { BodyWrapper } from "../components/BodyWrapper";
import styled from 'styled-components'
import Title from '../components/Title'
import { Beauty } from "../components/Beauty";
import { Cosmetolog } from "../components/Cosmetolog";
import { Research } from "../components/Research";
import { Undefined } from "../components/Undefined";
import { NextSeo } from 'next-seo';
import  Header   from '../components/Header'


export default class Home extends Component {
  render() {
    return (
      <BodyWrapper>
        <Head>
            <script src="https://identity.netlify.com/v1/netlify-identity-widget.js"></script>
        </Head>

        <NextSeo
              title="Home"
              description="Страница Марины Лагоды"
              keywords="biophysics, biology, medicine, cosmetology, lifescience, botany, ecology, landscape design, косметология"
          />

        <StyledHome>
          <Header />
          <Title />
          <main>
            <Cosmetolog />
            <Beauty />
            <Research />
            {/* <Undefined /> */}
          </main>

        </StyledHome>
      </BodyWrapper>
    )
  }
}

const StyledHome = styled.div`
    display: flex;
    flex-direction: column;
    main{
      display: flex;
      justify-content: space-around;
      flex-wrap: wrap;
    }
    h1{
      text-align: center;
      font-size: 30px;

    }

    h1,h3{
      font-family: 'Baskervville', san-serif;
      font-weight: 400;
      font-style: normal;
      letter-spacing: .11em;
      line-height: 1.2em;
      color: rgba(29,29,29,.7);
    }

    p{
    list-style-type: none;
    color: rgba(29,29,29,.7);
    font-weight: 400;
    font-style: normal;
    font-size: 15px;
    letter-spacing: .03em;
    line-height: 1.4em;
    }

    img{
      width: 280px;
      height: 300px;
    }
`
