import Head from 'next/head'
import { BodyWrapper } from '../components/BodyWrapper';
import styled from 'styled-components'
import { attributes, react as HomeContent } from '../content/resume.md';
import { Component } from 'react'
import { NextSeo } from 'next-seo';
import  HeaderResume  from '../components/HeaderResume'

export default class Resume extends Component {
  render() {
    let { title, thumbnail } = attributes;

    return (
      <BodyWrapper>
          <Head>
            <script src="https://identity.netlify.com/v1/netlify-identity-widget.js"></script>
          </Head>

          <NextSeo
              title="Resume"
              description="A short description goes here."
          />

          <StyledResume>
            <HeaderResume />
            <h1>{title}</h1>
            <main>
              <img src={thumbnail} />
              <HomeContent />
            </main>
          </StyledResume>
      </BodyWrapper>
    )
  }
}

const StyledResume = styled.div`
    color: rgba(29,29,29,.7);
    font-weight: 400;
    font-style: normal;
    font-size: calc(15px + 0.2vw);
    letter-spacing: .04em;
    line-height: 1.4em;
    display: flex;
    flex-direction: column;
    width: 100%;
    margin: 0 auto;
    text-align: justify;
  
    main{
      display: flex;
      margin-top: 40px;
      a{
      text-decoration: none;
      font-weight: bold;
      color: rgba(29,29,29,.7);
      }
      a:hover{
        color: rgba(29,29,29, 0.9);
      }
      div{
        margin-left: 3%;
      }

      @media screen and (max-width: 1000px){
        flex-direction: column;
      }
    }

    h1{
      /* margin: 40px; */
      font-family: 'Baskervville', san-serif;
      font-weight: 400;
      font-style: normal;
      font-size: calc(30px + 0.5vw);
      letter-spacing: .11em;
      line-height: 1.2em;
      text-align: center;
      width: 100%;
    }

    ul{
      padding: 10px;
    }

    img{
      max-width: 480px;
      object-fit: contain;
      align-self: flex-start;

    @media screen and (max-width: 1000px){
        margin: 0 auto;
      }

    @media screen and (max-width: 500px){
      max-width: 280px;
      }
    }

    blockquote{
      margin: 10px;
      text-align: justify;

    @media screen and (min-width: 500px){
      margin-left: 20px;
      }
    }
`
